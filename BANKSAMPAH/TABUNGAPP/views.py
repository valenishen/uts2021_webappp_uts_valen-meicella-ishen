from django.shortcuts import render
from django.http import HttpResponse
from .models import setoran

# Create your views here.
import django_tables2 as tables

class SimpleTable(tables.Table):
   class Meta:
      model = setoran

# this will render table
class TableView(tables.SingleTableView):
   table_class = SimpleTable
   queryset = setoran.objects.all()
   template_name = "listpage.html"