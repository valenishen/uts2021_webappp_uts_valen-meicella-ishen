from django.db import models

app_label = 'TABUNGAPP'

# Create your models here.
class jenisSampah(models.Model):
    nama = models.CharField(max_length=50)
    desc = models.CharField(max_length=200, default="")
    poin = models.IntegerField(default=0)
    gambar = models.ImageField(upload_to="images_sampah")
    pilihan_jenis = [
        ('Kertas', 'Kertas'),
        ('Botol', 'Botol'),
        ('Kaca', 'Kaca'),
        ('Aluminium', 'Aluminium'),
    ]

    jenis = models.CharField(
        max_length=20,
        choices=pilihan_jenis,
    )

    def __str__(self):
        return self.nama

class setoran(models.Model):
    waktu_setor = models.DateTimeField()
    penyetor = models.CharField(max_length=50)
    jenis_sampah = [
        ('Kertas', 'Kertas'),
        ('Botol', 'Botol'),
        ('Kaca', 'Kaca'),
        ('Aluminium', 'Aluminium'),
    ]

    jenis = models.CharField(
        max_length=20,
        default="",
        choices=jenis_sampah,
    )

    jumlah_setoran = models.IntegerField(default=0)
    total_poin = models.IntegerField(default=0)
    pilihan_status = [
        ('Menunggu Validasi', 'Menunggu Validasi'),
        ('Approved', 'Approved'),
    ]

    status = models.CharField(
        max_length=50,
        default="",
        choices=pilihan_status
    )

    def __str__(self):
        return self.penyetor
